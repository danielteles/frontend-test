import React from "react";
import PropTypes from "prop-types";
import Todo from "./Todo";

const TodoList = ({ todos, updateTodo }) => (
  <ul>
    {todos.map((todo) => (
      <Todo key={todo.id} todo={todo} {...todo} />
    ))}
  </ul>
);

TodoList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.number,
      completed: PropTypes.bool,
      inserted_at: PropTypes.string,
      updated_at: PropTypes.string,
    }).isRequired
  ).isRequired,
  updateTodo: PropTypes.func.isRequired,
};

export default TodoList;
