import React from "react";
import PropTypes from "prop-types";
import CompleteTodo from "../containers/CompleteTodo";
import RemoveTodo from "../containers/RemoveTodo";

const Todo = ({ todo }) => (
  <li
    style={{
      textDecoration: todo.completed ? "line-through" : "none",
    }}
  >
    {todo.name}
    <CompleteTodo todo={todo}></CompleteTodo>
    <RemoveTodo id={todo.id}></RemoveTodo>
  </li>
);

Todo.propTypes = {
  todo: PropTypes.shape({
    name: PropTypes.string,
    id: PropTypes.number,
    completed: PropTypes.bool,
    inserted_at: PropTypes.string,
    updated_at: PropTypes.string,
  }).isRequired,
};

export default Todo;
