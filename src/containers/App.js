import React, { Component } from "react";
import { connect } from "react-redux";
import Footer from "../components/Footer";
import AddTodo from "./AddTodo";
import VisibleTodoList from "./VisibleTodoList";
import axios from "../utils";
import { fetchTodos } from "../actions";

class App extends Component {
  componentDidMount() {
    axios
      .get("/api/todos")
      .then((response) => {
        this.props.fetchTodos(response.data);
      })
      .catch((error) => {
        console.log("App -> componentWillMount -> error", error);
      });
  }

  render() {
    return (
      <div>
        <AddTodo />
        <VisibleTodoList />
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = ({ data = {} }) => ({
  data,
});
export default connect(mapStateToProps, {
  fetchTodos,
})(App);
