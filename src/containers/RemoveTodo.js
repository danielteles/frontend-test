import React from "react";
import { connect } from "react-redux";
import { deleteTodo } from "../actions";

const RemoveTodo = ({ dispatch, id }) => {
  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          dispatch(deleteTodo(id));
        }}
      >
        <button type="submit">Delete Todo</button>
      </form>
    </div>
  );
};

export default connect()(RemoveTodo);
