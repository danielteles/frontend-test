import React from "react";
import { connect } from "react-redux";
import { completeTodo } from "../actions";

const CompleteTodo = ({ dispatch, todo }) => {
  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          dispatch(completeTodo(todo));
        }}
      >
        <button type="submit">Complete Todo</button>
      </form>
    </div>
  );
};

export default connect()(CompleteTodo);
