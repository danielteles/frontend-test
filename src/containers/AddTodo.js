import React from "react";
import { connect } from "react-redux";
import { addTodo } from "../actions";
import axios from "../utils";

const AddTodo = ({ dispatch }) => {
  let input;
  let todo = {
    completed: false,
    id: 0,
    inserted_at: "",
    name: "",
    updated_at: "",
  };

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          if (!input.value.trim()) {
            return;
          }
          axios
            .post("/api/todos", { name: input.value })
            .then((response) => {
              todo = { ...response.data };
              console.log("Todo", todo);
              dispatch(addTodo(todo.name));
            })
            .catch((error) => {
              console.log("Error", error);
            });

          input.value = "";
        }}
      >
        <input ref={(node) => (input = node)} />
        <button type="submit">Add Todo</button>
      </form>
    </div>
  );
};

export default connect()(AddTodo);
