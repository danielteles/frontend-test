const todos = (state = [], action) => {
  switch (action.type) {
    case "FETCH_TODOS":
      return [...state, ...action.todos];
    case "CREATE_TODO":
      return [
        ...state,
        {
          id: action.id,
          name: action.name,
          completed: false,
        },
      ];
    case "UPDATE_TODO":
      return state.map((todo) =>
        todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      );
    case "DELETE_TODO":
      return state.map((todo) => (todo.id === action.id ? [] : todo));
    default:
      return state;
  }
};

export default todos;
