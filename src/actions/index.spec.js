import * as actions from "./index";

describe("todo actions", () => {
  it("createTodo should create CREATE_TODO action", () => {
    expect(actions.createTodo("Use Redux")).toEqual({
      type: "CREATE_TODO",
      id: 0,
      text: "Use Redux",
    });
  });

  it("setVisibilityFilter should create SET_VISIBILITY_FILTER action", () => {
    expect(actions.setVisibilityFilter("active")).toEqual({
      type: "SET_VISIBILITY_FILTER",
      filter: "active",
    });
  });

  it("updateTodo should create UPDATE_TODO action", () => {
    expect(actions.updateTodo(1)).toEqual({
      type: "UPDATE_TODO",
      id: 1,
    });
  });
});
