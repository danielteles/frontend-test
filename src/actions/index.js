import axios from "../utils";

export const retrieveTodos = (data) => ({
  type: "FETCH_TODOS",
  todos: data,
});

const createTodo = (text) => ({
  type: "CREATE_TODO",
  text,
});

const updateTodo = (id) => ({
  type: "UPDATE_TODO",
  id,
});

const removeTodo = (id) => ({
  type: "DELETE_TODO",
  id: id,
});

export const setVisibilityFilter = (filter) => ({
  type: "SET_VISIBILITY_FILTER",
  filter,
});

export const VisibilityFilters = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_COMPLETED: "SHOW_COMPLETED",
  SHOW_ACTIVE: "SHOW_ACTIVE",
};

export function fetchTodos() {
  return function (dispatch) {
    return axios
      .get("/api/todos")
      .then((response) => {
        console.log("fetchTodos -> response", response);
        dispatch(retrieveTodos(response.data));
      })
      .catch((error) => {
        console.log("App -> componentWillMount -> error", error);
      });
  };
}

export function addTodo(text) {
  return function (dispatch) {
    return axios
      .post("/api/todos", { name: text })
      .then((response) => {
        dispatch(createTodo(text));
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };
}

export function completeTodo(todo) {
  return function (dispatch) {
    axios
      .put(`/api/todos/${todo.id}`, {
        name: todo.name,
        completed: !todo.completed,
      })
      .then((response) => {
        dispatch(updateTodo(todo.id));
        fetchTodos();
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };
}

export function deleteTodo(id) {
  return function (dispatch) {
    return axios
      .delete(`/api/todos/${id}`)
      .then((response) => {
        dispatch(removeTodo(id));
        fetchTodos();
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };
}
